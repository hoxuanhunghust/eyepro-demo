<!DOCTYPE html>
<html lang="zxx" class="js">
<?php
session_start();
// echo $_SESSION["schoolId"];
?>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Giải pháp công nghệ 4.0 cho giáo dục. 
	-Hệ thống trắc nghiệm ONLINE
	- Hệ thống dạy học đa phương tiện ONLINE
	- Hệ thống kiểm soát học sinh từ xa ứng dụng trí tuệ nhân tạo AI">
	<!-- Fav Icon  -->
	<!-- <link rel="shortcut icon" href="images/favicon.png"> -->
	<link rel="shortcut icon" type="image/jpg" href="https://vdsmart.vn/wp-content/uploads/2019/12/logo-1.png" />
	<!-- Site Title  -->
	<title>EyePro Education - Công ty Cổ phần Công nghệ VDSMART</title>
	<!-- Vendor Bundle CSS -->
	<link rel="stylesheet" href="assets/css/vendor.bundle.css_ver=130.css">
	<!-- Custom styles for this template -->
	<link href="assets/css/style.css_ver=130.css" rel="stylesheet">
	<link href="assets/css/theme-purple.css_ver=130.css" rel="stylesheet" id="layoutstyle">
	<script>
		(function(i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function() {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-91615293-2', 'auto');
		ga('send', 'pageview');
	</script>
</head>

<body class="overflow-scroll">

	<!-- Start .header-section -->
	<div id="home" class="header-section flex-box-middle section gradiant-background header-curbed-circle background-circles header-software">
		<div id="particles-js" class="particles-container"></div>
		<div id="navigation" class="navigation is-transparent" data-spy="affix" data-offset-top="5">
			<nav class="navbar navbar-default">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#site-collapse-nav" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.html">
							<img class="logo logo-light" src="images/logo_eyepro.png" alt="logo" />
							<img class="logo logo-color" src="images/logo_eyepro.png" alt="logo" />
						</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse font-secondary" id="site-collapse-nav">
						<ul class="nav nav-list navbar-nav navbar-right">
							<li><a class="nav-item" href="https://falion.csam.com.vn/registration" target="_blank">1. Đăng ký tài khoản</a></li>
							<li><a class="nav-item" href="main.php#demo">2. Điểm danh ONLINE</a></li>
							<li class="demo-dropdown dropdown">
								<a href="#home" class="nav-item dropdown-toggle" data-toggle="dropdown">3. EyePro</a>
								<div class="dropdown-menu" style="width: auto; left: 0px;">
									<ul style="float: right; width: 100%;">
										<li>
											<img src="images/screen.jpg" width="250px" height="200px" alt="desktop-app">
											<span>EyePro</span>
											<span class="new-badge">new</span>
										</li>
										<li><a href="https://eyepro.vn/" target="_blank">Trang chủ</a></li>
										<li><a href="https://eyepro.vn/signin" target="_blank">Đăng nhập</a></li>
										<li><a href="https://eyepro.vn/signup" target="_blank">Đăng ký tài khoản</a></li>
										<!-- <li><a href="index-x4-particles.html">Tạo lớp học</a></li> -->
									</ul>
								</div>
							</li>
							<li><a class="nav-item" href="main.php#team">Đội ngũ phát triển</a></li>
							<li><a class="nav-item" href="main.php#contact">Liên hệ</a></li>
							<li><a href="/eyepro-demo" class="nav-item">Đăng xuất</a></li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container -->
			</nav>
		</div><!-- .navigation -->

		<div id="demo" class="header-content pt-90">
			<div class="container">
				<div class="row text-center">
					<div class="col-md-8 col-md-offset-2">
						<div class="header-texts">
							<h1 class="cd-headline clip is-full-width wow fadeInUp" data-wow-duration=".5s">
								<span>
									<!-- <?php echo $_SESSION["schoolId"]; ?> -->
									EyePro - </span>
								<span class="cd-words-wrapper">
									<b class="is-visible">Face Recoginition</b>
									<b>Báo cáo</b>
									<b>Online 24/7</b>
								</span>
							</h1>
							<ul class="buttons">
								<li><a href="#" class="button wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".6s" id="btn_webcam">Start Webcam</a></li>
								<li><a href="#" class="button button-border button-transparent wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".9s" id="btn_screen">Chia sẻ màn hình</a></li>
							</ul>
						</div>
					</div><!-- .col -->
				</div><!-- .row -->
				<div class="row text-center">
					<div class="col-md-10 col-md-offset-1">
						<div class="header-mockups">
							<div class="header-laptop-mockup black wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s">
								<video poster="images/vds.png" id="video" class="col-8" width="640" height="480" autoplay muted></video>
								<!-- <img src="images/software-screen-a.jpg" alt="software-screen" /> -->
							</div>
							<div class="iphonex-flat-mockup wow fadeInUp" data-wow-duration="1s" data-wow-delay=".9s" style="border-radius: 18px; visibility: visible; animation-duration: 1s;animation-delay: 0.9s; animation-name: fadeInUp;">
								<img src="images/default_user.png" alt="Face detected" style="padding-top: 75px;" id="face" />
								<!-- <img src="images/app-screen-a.jpg" alt="app screen"> -->
							</div>
							<div class="row" id="divResult" style="padding-top:10" hidden>
								<img src="" alt="Screen shot" style="border:1px solid black" width="480" height="360" id="screenshot" />
								<canvas id="canvas" style="overflow:auto"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div><!-- .container -->
		</div><!-- .header-content -->
	</div><!-- .header-section -->


	<!-- Start .about-section  -->
	<div id="about" class="about-section section white-bg">
		<div class="container tab-fix">
			<div class="section-head text-center">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h2 class="heading">Về sản phẩm <span> EyePro</span></h2>
						<p>Eye Pro Education: Giải pháp công nghệ 4.0 cho giáo dục</p>
					</div>
				</div>
			</div><!-- .section-head -->
			<div class="row tab-center mobile-center">
				<div class="col-md-6">
					<div class="video wow fadeInLeft" data-wow-duration=".5s">
						<img src="images/video.png" alt="about-video" />
						<div class="video-overlay gradiant-background"></div>
						<a href="https://www.youtube.com/watch?v=2zvbYJRt2pc" class="video-play" data-effect="mfp-3d-unfold"><i class="fa fa-play"></i></a>
					</div>
				</div><!-- .col -->
				<div class="col-md-6">
					<div class="txt-entry">
						<h4>Hệ thống trắc nghiệm ONLINE.</h4>
						<p>Hệ thống dạy học đa phương tiện ONLINE.</p>
						<p>Hệ thống kiểm soát học sinh từ xa ứng dụng trí tuệ nhân tạo AI.</p>
						<p><b>Đơn vị thực hiện:</b> Công ty CP Công nghệ VDSmart <a href="https://vdsmart.vn">https://vdsmart.vn</a> là Công ty công nghệ chuyên xây dựng các giải pháp ứng dụng trí tuệ nhân tạo AI trên IoT – Internet kết nối vạn vật (AI over IoT) ứng dụng trong các lĩnh vực như Giáo dục thông minh (Smart Education), tòa nhà thông minh (Smart Building), Ngân hàng, Bán lẻ (Smart Retail), v.v. </p>
						<a href="https://vdsmart.vn" class="button">VDSmart</a>
					</div>
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- .about-section  -->


	<!-- Start .testimonial-section  -->
	<div id="team" class="testimonial-section section white-bg pb-120">
		<div class="imagebg">
			<img src="images/testimonial-bg.png" alt="testimonial-bg">
		</div>
		<div class="container">
			<div class="section-head text-center">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h2 class="heading">Niềm tự hào <span> của chúng tôi!</span></h2>
						<p>VDS EyePro Education là hệ thống đào tạo trực tuyến và quản lý học sinh từ xa dùng công nghệ nhận dạng khuôn mặt 4.0 hiện đại nhất với lõi trí tuệ nhân tạo. Giải pháp cho phép Nhà trường luôn nắm được tình trạng và thái độ học tập của học sinh kể cả khi đang học tập trực tuyến.</p>
						<!-- <p>VDSMART chuyên nghiên cứu ứng dụng trí tuệ nhân tạo AI trong thị giác máy tính với sản phẩm VDS Face là hệ thống điểm danh sinh trắc học dùng công nghệ nhận dạng khuôn mặt 4.0 hiện đại nhất với lõi trí tuệ nhân tạo. VDS Face cho phép nhà trường luôn nắm được tình trạng sĩ số và thái độ học tập của học sinh, giúp cha mẹ biết được tình hình con đến trường và hoạt động học tập. Hệ thống còn có thể giúp cảnh báo an ninh và nhiều tính năng cao cấp khác nữa (nhận dạng trạng thái & mức độ tích cực học tập, v.v.).</p> -->
					</div>
				</div>
			</div><!-- .section-head  -->

			<!--<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="testimonial-carousel has-carousel" data-items="1" data-loop="true" data-dots="true" data-auto="true" data-navs="false">
						<div class="item text-center">
							<div class="quotes">
								<img src="images/quote-icon.png" class="quote-icon" alt="quote-icon" />
								<blockquote>Hãy tư duy như người lái xe tăng: vượt qua mọi trở ngại!</blockquote>
								<h6>PGS. TS Nguyễn Chấn Hùng</h6>
								<div class="client-image">
									<img src="images/admin.jpg" alt="client" />
								</div>
							</div>
						</div>
						<div class="item text-center">
							<div class="quotes">
								<img src="images/quote-icon.png" class="quote-icon" alt="quote-icon" />
								<blockquote>Tôi tin rằng khoảng một nửa những gì khác biệt giữa doanh nhân thành đạt và không thành đạt là sự kiên trì tuyệt đối.</blockquote>
								<h6>Hồ Xuân Hùng</h6>
								<div class="client-image">
									<img src="images/hunghx.jpg" alt="client" />
								</div>
							</div>
						</div>
						<div class="item text-center">
							<div class="quotes">
								<img src="images/quote-icon.png" class="quote-icon" alt="quote-icon" />
								<blockquote>Những giấc mơ sẽ không bao giờ trở thành hiện thực nếu bạn không đi tìm chúng.</blockquote>
								<h6>Nguyễn Văn Giảng</h6>
								<div class="client-image">
									<img src="images/giangnv.jpg" alt="client" />
								</div>
							</div>
						</div>
						<div class="item text-center">
							<div class="quotes">
								<img src="images/quote-icon.png" class="quote-icon" alt="quote-icon" />
								<blockquote>Đôi khi một khoảnh khắc sáng suốt có giá trị bằng cả kinh nghiệm của một cuộc sống.</blockquote>
								<h6>Nguyễn Việt Hưng</h6>
								<div class="client-image">
									<img src="images/hungnv.jpg" alt="client" />
								</div>
							</div>
						</div>-->
			<!-- <div class="item text-center">
							<div class="quotes">
								<img src="images/quote-icon.png" class="quote-icon" alt="quote-icon" />
								<blockquote>Nam et sagittis diam. Sed tempor augue sit amet egestas scelerisque. Orci varius natoque penatibus et magnis dis parturient montes nascetur.</blockquote>
								<h6>Andy Lovell</h6>
								<div class="client-image">
									<img src="images/client-1.jpg" alt="client" />
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>  -->

		</div><!-- .container  -->
	</div><!-- .testimonial-section  -->


	<!-- Start .contact-section  -->
	<div id="contact" class="contact-section section gradiant-background pb-90">
		<div class="container">
			<div class="section-head heading-light text-center">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h2 class="heading heading-light">Liên hệ với chúng tôi</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="contact-form white-bg text-center">
						<h3>Thông tin</h3>
						<p>Nếu quan tâm đến sản phẩm của công ty VSMART vui lòng liên hệ với chúng tôi qua email hoặc qua form biểu mẫu:</p>
						<form id="contact-form" class="form-message" action="http://demo.themenio.com/appsland/form/contact.php" method="post">
							<div class="form-results"></div>
							<div class="form-group row fix-gutter-10">
								<div class="form-field col-sm-6 gutter-10 form-m-bttm">
									<input name="contact-name" type="text" placeholder="Họ và tên" class="form-control required">
								</div>
								<div class="form-field col-sm-6 gutter-10">
									<input name="contact-email" type="email" placeholder="Email" class="form-control required email">
								</div>
							</div>
							<div class="form-group row fix-gutter-10">
								<div class="form-field col-md-6 gutter-10 form-m-bttm">
									<input name="contact-phone" type="text" placeholder="Số điện thoại" class="form-control required">
								</div>
								<div class="form-field col-md-6 gutter-10">
									<input name="contact-subject" type="text" placeholder="Chuyên nghành" class="form-control required">
								</div>
							</div>
							<div class="form-group row">
								<div class="form-field col-md-12">
									<textarea name="contact-message" placeholder="Lời nhắn" class="txtarea form-control required"></textarea>
								</div>
							</div>
							<input type="text" class="hidden" name="form-anti-honeypot" value="">
							<button type="submit" class="button solid-btn sb-h">Gửi thông tin</button>
						</form>
					</div>
				</div><!-- .col  -->
				<div class="col-md-6">
					<div class="contact-info white-bg">
						<div class="row">
							<div class="col-sm-6">
								<h6><em class="fa fa-envelope"></em> info@vdsmart.vn</h6>
							</div>
							<div class="col-sm-6">
								<h6><em class="fa fa-phone"></em> +84904186221</h6>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<h6><em class="fa fa-map-marker"></em>Tòa nhà HITECH, số 1 Đại Cồ Việt, Quận Hai Bà Trưng, Hà Nội</h6>
							</div>
						</div>
					</div>
					<div id="gMap" class="google-map"></div>
				</div><!-- .col  -->
			</div><!-- .row  -->
		</div><!-- .container  -->
	</div><!-- .contact-section  -->


	<!-- Start .footer-section  -->
	<div class="footer-section section">
		<div class="container">
			<div class="row text-center">
				<div class="col-md-12">
					<!-- <ul class="footer-navigation inline-list">
							<li><a class="nav-item" href="index-x3.html#home">Home</a></li>
							<li><a class="nav-item" href="index-x3.html#about">About</a></li>
							<li><a class="nav-item" href="index-x3.html#features">Features</a></li>
							<li><a class="nav-item" href="index-x3.html#pricing">Pricing</a></li>
							<li><a class="nav-item" href="index-x3.html#team">Team</a></li>
							<li><a class="nav-item" href="index-x3.html#testimonial">Testimonial</a></li>
							<li><a class="nav-item" href="index-x3.html#contacts">Contacts</a></li>
						</ul> -->
					<ul class="social-list inline-list">
						<li><a href="https://vdsmart.vn/"><em class="fa fa-facebook"></em></a></li>
						<li><a href="https://vdsmart.vn/"><em class="fa fa-twitter"></em></a></li>
						<li><a href="https://vdsmart.vn/"><em class="fa fa-google-plus"></em></a></li>
						<li><a href="https://vdsmart.vn/"><em class="fa fa-pinterest"></em></a></li>
						<li><a href="https://vdsmart.vn/"><em class="fa fa-linkedin"></em></a></li>
						<li><a href="https://vdsmart.vn/"><em class="fa fa-instagram"></em></a></li>
					</ul>
					<ul class="footer-links inline-list">
						<li>Copyright © 2020 VDSMART</li>
						<li><a href="#">Privacy Policy</a></li>
					</ul>
				</div><!-- .col  -->
			</div><!-- .row  -->
		</div><!-- .container  -->
	</div><!-- .footer-section  -->

	<!-- Preloader !remove please if you do not want -->
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!-- Preloader End -->

	<!-- Google Map Script -->
	<script src="https://maps.google.com/maps/api/js?key=AIzaSyBg4EFCEar5hA3IMA9zdTWi6TVdyY5RU3c"></script>
	<script src="assets/js/googleMap.js"></script>

	<!-- JavaScript
		================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<script src="assets/js/jquery.bundle.js"></script>
	<script src="assets/js/script.js"></script>

</body>

<script>
	/**
	 * this script is for face detetion and screenshoot
	 * if system detect face front of camera
	 * then he take a screen shot of an active windows
	 */
	const video = document.getElementById('video');

	const screenEl = document.createElement('video');
	screenEl.id = "screen";
	screenEl.autoplay = "autoplay";

	// document.getElementById('divResult').appendChild(screenEl);
	const btn_webcame = document.getElementById("btn_webcam");
	const btn_screen = document.getElementById("btn_screen");
	const screen = screenEl;
	const canvas_screen = document.getElementById('canvas');
	const image_face = document.getElementById('face');
	const screen_shot = document.getElementById('screenshot');

	var display_surface = "";
	var nbr_face = 0;

	var webcam_is = false;
	var screen_is = false;

	var face_data = null;
	var screen_data = null;

	const constraints = {
		video: true
	};
	const display_media_options = {
		video: {
			cursor: "never",
			width: "640",
			height: "480"
		},
		audio: false
	};
	/**
	 * loading a model for face detection
	 * We use Tiny Face Detector
	 */
	async function loadModel() {
		Promise.all([
			faceapi.nets.tinyFaceDetector.loadFromUri('/eyepro-demo/models')
		]).then(startVideo)
	}
	/**
	 * start video stream from web webcame
	 */
	function startVideo() {
		if (navigator.mediaDevices.getUserMedia) {
			navigator.mediaDevices.getUserMedia(constraints).
			then(handleSuccess).catch(handleError);
		}
	}

	function handleSuccess(stream) {
		video.srcObject = stream;
		btn_webcame.innerHTML = "Stop sharing";
	}

	function handleError(error) {
		alert("Please give access to your Webcam to enjoy this course !");
	}

	btn_webcame.addEventListener("click", function(event) {
		event.preventDefault();
		/**
		 * check if the webcam is open or not
		 * and change the status of webcam_is
		 */
		if (!webcam_is) {

			loadModel();
			webcam_is = true;
		} else {
			var stream = video.srcObject;
			var tracks = stream.getTracks();

			for (var i = 0; i < tracks.length; i++) {
				var track = tracks[i];
				track.stop();
			}
			video.srcObject = null;
			webcam_is = false;
			this.innerHTML = "Share webcam";
		}

		console.log("Webcam btn click");
	});
	btn_screen.addEventListener("click", function(event) {
		event.preventDefault();
		/**
		 */
		if (!screen_is) {
			shareScreen();
			screen_is = true;
		} else {}
		console.log("Screen btn click");

		setInterval(async () => {
			if (screen_is === true) {
				canvas_screen.width = screen.videoWidth;
				canvas_screen.height = screen.videoHeight;
				canvas_screen.getContext('2d').drawImage(screen, 0, 0, screen.videoWidth, screen.videoHeight);
				screen_shot.src = canvas_screen.toDataURL('image/webp');
				screen_data = canvas_screen.toDataURL("image/png");
				console.log("width: " + canvas_screen.width);
				console.log("height: " + canvas_screen.height);
			} else {
				screen_data = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAAA8CAYAAADxJz2MAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAABIuSURBVHhe7Vl7kJbVef/u3+6yu8ByWVAgGTOikERREQEVohFF02k0FjWm1SaxiZqLl1YTM+qgiUanNUmNk8lo06TttDHTpNWAUVBkuSy7wC7ihapRbgJy84aKcvXX3+95znnf71s/O9+k/37M/Hjf9znP7fzOc55zPsjwDxr4f6GmsIH6UVPYQP2oKWygftQUNlA/agobqB81hQ3Uj5rCBupHTWED9aOmsIH6UVPYQP2oKWygftQUNlA/agobqB81hQ3Uj8qPXPKeDaj5ndVTuh+tbzqmVwupXUS0T/0E/9HPR/irikn9bKXvATbV/lNZfP+/4blU2ZvvJIgCF8Izgzyf+QrFAp+C6eYkL9CR9N2hdIXEeU4BNaEg+4g4elbaK4bHpU6WkJ88v/PuL8arjOvf7jdveUlXNoRy5btkmlPBnm4veWKf5Jf6T2BjysdtU3vFTAJEQjzJRDE4TQiUvhGoiUvfA0b9KudGYEg4SbAYEPQq4kXy3Id8i0C+hxxjLpVQbJ+w64sk+5ZNBYEaU761CLTvCv9VBEa5ESj/MT/JlHswKlhwKfHbCIpKejpZycqGcX0nwSxI5YTk3FfdvhO75oAKGZHaBbmRHPIhypQLPu5kWYzEb5S5vsnMj6BcyoaYc0RS8Ym+xqt10pjFATJ7uqBEI61O7eCajBLgU3IidVRtI1lCmunI3scMH0FgtHUdj5kNCWuCTYQTqHixivke7SOB0WfwJ1vfKdX5x3knBJpccd2PxiKchwr/wT7MwR2UwtOCq/Sp5Cshw5iwEnAnifNQ2mnPq6hUg+y99JWs+xPiuCP6M58hpgiUTTJJwWIJfE9ylb6qKxBkhLqNb9lAjAiKdkKI674jeZpnWoFOVMVOirZxXMYRyaRMQQ5l5M/EuVafCScBlLzJKvX4jH5scqpuX6SYVLQfiJiw+xnQc8wvocWKE7EYytMJjyTJl9vGycs26Ff1xhib49G/5ETMR2NVlRpsbVxCezEjOfFmm5AQYQG0OmEr2LfLFSAGS/xU2cbJ8D0Jrol53/XJy68WI9WpeqetYiR+LL7nEOVOkseWzHNKF8K+jWBBscI8w5iPEzVixsMn0UljhY8cJ5AtG9PqNQOdxa3kFRdJ9EqMNqows7GFiOC3wfXNH6FkpK/em7exQQR7o8YLriNfWfNHmK+0FVTCJ+bjvksoEyj3uM18dwKinwQVO0SwWJYj9TgPyeLc9J7wYuTJVoI8E88PY+JD0MzvNqJsTpTMED4HoSXIzbhQon4bjZWYN/gOyjVuSTDpbI46liwhP5lW2rivTL5oE5Sd/BrxmXb6HsoxLk6RiXGbRd82XuR4nj7oN0db5TmI8MWmf41l6TvbYpNsYf5mZ/GGU1YKBHqO7YR8GwmFQuLPdDT3Up7yPOV5k7dlssFfgBWSeMhTMUsC245FV/8mYO9uXHreSeY8m81SuRM3zf0JsGcTJo/j5GTMZC+97Ot45unngUMHgQ/ew46X1+Dmay7HEE4uVnCuVEaOZFnFFkdg4vTzsObZF/HB4f0A3sHWP67BMaOGkUTmkO/A+ONnoO+557H34H4cosahAwdx2w03YGhTM5b0rsFeyhgNhz/Yj62b12LW6ZOYZw6thaFY3tMHef3gA1Og8U5898arkGkehSdWb+DAYRq/zcEDfD+I7175DQwttaJ7zTrsOXgIF39uppFaFEmlJnzr+3Px2sF9+MTwToygTCRacdj8Cca1hcuRWVu5lomY37uZAfbh9Q09+Hh7XPnRuO6We4C3SeAIEToE1975AA4xoU3PP4Obrr8JV1/5t+hZSzI5vduvuhBjGCQJliM5rILOk2ZjK3PfsWEdLj7zBMyeORW/+dW/YGLnWJJQwLnnX4pd7xzAq5s34JYbrsdlX/4a7rrnp/jxPfeirVxGT88idK94EtMmT8fMmWfguY1Pk+g9mDDqWHTmhuLpVV1YuuwRzJh2Ok6fOgMzTzuFE2X1Dx2Px1a/jFVLfo9zTx6LM6Yej2mnTOfkuYOKJLfvJbzPzLf9cQXGtcWqbcKVd3wfr3MZJx05CqMo006x7RvbS9xdVrL5wcgM+jTm9+3A6v5VeGvbU/jtA7dZJWUKo3HjbT8F3tqEUztZSa1jsW4PsHzxgrBl5Yhbs+0o/OLB/wLefA5TO70K/eeXnq049YvXYScL46LZp6KTY6rwAiuvxN7X0ToSO3btxPJlT2J0a8G3lvWnVuRy7SjmmtHfvwxdXX9AKdvEsSJumHst3t73Go4afjQ6Mh3o73kcK7rnoZxXn8zRRrlzi7Ucgz/0bcaapb+nXmgJWlTmnG8agyX9L2Jp32rs2vESfv3Le0PsHL5x9w+xG4dx3PBhGE2ZCMzF019zigR6z2L/aJ2IR9buxpLu5fjiBTNYiBtw4efP4iodge/N/RmwcxOmj2zCmV/6CtaTiL/8/DmWUNEIYn8qHoVZF10JvL8RX5090XqKTvOCkmWLyHYchxd3ANu3voTvfPMiIzinrcC+O3vOZbTbhr86dxJaKW+iz0Ke1W53u2EoM4eunqeIfpwy7WzM+uxsvLbrWfT3zkc520bChmFlfx8WLX/CJifyrI3If/txeGTlLvR0LbQ+LbmqrJX9u419d2XvcnStXI6zL74ce7hDLv2zMyz3q++4FdsPH8TxI49ICMxH8oicneLcws1q2iJw8Kcxr/9VLOl6HM1U+N1//hLr1m9EofNY3HTL3cBrmzClo4Bv3XYXtpDA48cOs95gC1BiBZcn4GOTzwXe24i/v+58D8gVslO6rAbfifEnzMJTzz7DDfMuNm/5H0z/7GeYQCeuufUOYP9GTBkTEiV0UOTstB+GUmkclq9eh72Mu484wIk+cN/3TNdO7vxI9PT3s7upd7LXaVMe2I6x40ZZXgv632JfZPfcv5Vjr+HQ3tfxySOPZc9rRl/vIixma8jkO/HzBx/Dtpf7Maojg2t/eAt2HzyAk0YeiSMYJ54Jto2VnwhUBdrvXxE4aAIWPr0Fz3TP99O2dQyeewO4+xcP4s475gJvvIApI0o476IroFZ8yaxTkq1oLaB0NE79whWc4Xp85axx1nSLhZyXvU4sO0GJQgs+OXkaul/YwB4DnPnnl+GC8+ewfW7AJbPHh0Tp01Zbp/lI+hmOnt6l6OrupZ/x+Kd/6+Ih8SomHKWTvozWto9hZfdSLFk8D9OnT8Wpp0/FSSccTR+MPWgiFvTtQt/iRzF78pE467QJOPGET9mcm1o6sWrtCjy+dCF1O1BuOxJbdm/Cz/71R/jOXbdi7/vv4sQO38LeG/NGXDabNyKNTFVJhn0m0/4pLFi9Ec8u/rVtzUyhE9Mu+juQQyxYOJ8EPo+TR5VQaj0CL21/H08vfRxDqWe9jttIW/3+3zzECnwJJ4+uaLrmn6VeoI5dfVSNrTh6ymewex9w/fU3Y8qkSTjwzias6v5vFEQebez0tsUto6mkSnkCK3qW8nsMcq3HYMfuV7B86TxWa5lbtg1rVnajt5uVRFvdLGxnqAe3TsCjfduxZskC2zHanqruDA+RXGkouvuX4snlC5HToZIZjHPmzMGbrOWHn3wE+/fvwYmdQ6xQrCXwKpTcZ+VHLcKY5YQyrSfgsZVbsO7J32KkyRicJ/B9Dy6zbYO3N2PyaBqyKm68ndca7pQlj3bh61/7Nq765nXoW/ssld7D7dddbAsgAo0M3TF5cp91wRV4aNFanHHOHMyccTbmP/Qwt9R7+IvzZtuWvfkHd9pp+PwL63GDTvavXo37//Fe/PzHd6G9lMEzqxahr3uRtQVV87fn/gPeYg6XfO4LGEz73t4VWNzdjWnTZmHG1NNwzinHYHgHJ9v+CcxbsxP9S5Zh9qTxOPe0yZgxZTI6B41Ae7EFffS7rPvRQA4PQ96H73/wYV6ZDmPv/l2YMKYVQ+h/EOM2ZVoYn/PR/U87xHo4B62P8BrzRO8mPLdknh3bfqEksUMmYuOrLJW3tuP4I5pR4KVT15LLv3wtNm54w+5dhw4dwrYNL2AO71LaukPoWH3UKlB9jASOP+lsXj322D1Of73+yibcceM1RnRZlcJdcOFfX4v1r/CkkRIvgm9u2YJr/uZLvAdmsJoVv7KLJ78Sl9/BH8eGN4Gt617AsSOGoq+vD+/Sxu+BxL4d+I9f3Wc9duG6XdYWtWA4yBf2w9/987+jndtxde8CrFzFw8cWpoPPEci2jcX6ndt4oXsT4zqLdrC188bQQp5U8XZ4KA+70thtusUmoPugSlWT8tNVTLciy18B6k1WVUReP/vCTy/1BJFdon4znaqiBVsAoSACefXQZV1PboESZfLTzDtombDf3oqvXw05b9Syl47yEXRdKsZfA0JJLWFQkpctlmLRV5G7x7ccZU3sz7wllFhhg2gvn0LMM+m30hU5+kXElmC/hihvL3ufF8ok0X9v62YhwrWVjQwxqr3tvxd1G1dw/yWi6sibXBO2wHaPoj7Jl7yl6ImJdJs8e5AfHkoqPAnJTE6dXEjG/0FBOfDKUuAKc1U1KXvKl0GnOX/VsJolK5U0OaLYZOOKneeEom/pFH1yjCvdIuPx5yO/dT2yceon1xI+85KLGO1G26aet/xHsvVuRBvhslfeFoQIl15NqshJKYiUS01i242VQJNde6jLStG4fjPLuSah75zG7XciK4E/iSSTrzK3tXSi36xt26xVsPz5vyN6HGvyAU40F0u7xIj2PAs84eVH0HskxuSyVVXb4njMrOIYQW7jOfApMgKRphfsCrqE27fnbWTzaX3PdLlg1jcVrMKJjG0yuvNQpmpwx2Rc210BTC917hVQ8JNTlSxo+xsxJJhEqaqlV2K1Fkuq7DRmqVSyPpj4irlYRWlCqvbomz6pW6KOVYT0WACeY/i2YqA/klDmPOQzxjMiNZey2hNlReXKfGhXIDleKGG+3EnmVzvR8gnge9w99OWCnJ4UJsmaEzrlmLZmIdnqXpHuxKEqcVtf/VwhVC0TUL+TTlIJhNnH1S/6tyYZe51Vgd0d5dcnnA09yQgkCaaviSkmc5Jf/erJFkWM7w7FlJ793rdYLo95ZlStWiTlzPGYZ0qidAnp2pYOdtKnT+lkNBHdZ6RsRKjKmLyqTwranmreRp4RSCPZ0EkaIBAo5yH5IldW4165quqwomE1C7EKdZLxWz+/rPpImF0pVMGWsHy7jukaqWm1ekPnVlUMvQdCVKEatxxM7j6Ut/VH5Sldvct30LU5c8xIpI33VfmUjYrL/afzD45j8r5V+OS3ZLEqrP/YFqI+4eS4bRVMriqKcFlCwoegmK7nq65YYVKCyQboG8mu7771rQXmWCBQY04wEWTSNWLMRjLFimNpvpVw/9JT3BAj2pgd/5Ki/5O1ElcSUooEst9EY8lrERgWwR3GRByeQMV40EmTiodOHIvjaQzBydWYJiG4vo9XEBgqJCFKPkN+0U9CIOE+BeXy4bhpPhoXFMdjhAMlOlYSIYFQtpKJPP2fsRkpETs902TMUSWSYO7DExiIqKNkQm8d4Cf6j0/fIbKVTarv4yIixgw+TJeoWlzXr0SMkeRc4dflgs/Fi6JCr5rAUDFJMCdU5NkWliwSWLHlEv3EzoM59D0QcaySQL1zzHz4uHzHyQkpgcEuxEtzr5ywj1UhyKNe9C+/bse4oXoF14u5pv6reBKBcTAm606ikTdrT5wy6yU+VplMTM4QE44IcteNthEiw1c08WPbWYvnJEafccJmZ31Q9v4d83AfwXcSP3zbWKoXkczbbFK7qlzNT9SvIFAFlQ5ER2lyHw4QxoLDqOPjQacSIZmom6JaryqOxVBlOonRRzoeEL8D9O7f0W+UVeec6hGUJd9h4VK9QFS0Dboi0M6KVB6dyEADVYM2ZsHiWJC7LEW0T31U+xlolyboMaq2qJEYEHZEkod81OrBleOExnx7Rl9pLPOl6on9Ud+xt1peTpTlp/EKPZHn/w2bzC04tABOQGIYkYxXyE2WopLAqqSlGxOosPO48unvVa3C2kS057OibUR/0k0IlP/E1mVOoOc0kMBKP/Y0mXTSyo99daBectjGWG4cUZHon4RoP8BPkuRHwwkN36Yf7fmstNc78WH9aqTjA3KJCH5SWdRz3cS3UKHni17lLx1s4E9CTWED9aOmsIH6UVPYQP2oKWygftQUNlA/agobqB81hQ3Uj5rCBupHTWED9aOmsIG6kMH/ArLczal/NW8vAAAAAElFTkSuQmCC"
			}
			requestXHR(face_data, screen_data);
		}, 300000)
	});

	video.addEventListener('play', () => {
		console.log("PLAYY!")
		const canvas_video = faceapi.createCanvasFromMedia(video);
		const displaySize = {
			width: video.width,
			height: video.height
		};
		faceapi.matchDimensions(canvas_video, displaySize);

		const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions());
		/**
		 * count number of face detected in the stream
		 */
		nbr_face = detections.length;
		const resizedDetections = faceapi.resizeResults(detections, displaySize);
		canvas_video.width = video.videoWidth;
		canvas_video.height = video.videoHeight;

		if (nbr_face > 0 || screen_is === true) {
			
			for (var i = 0; i < nbr_face; i++) {
				var _x = resizedDetections[i]["_box"]["_x"];
				var _y = resizedDetections[i]["_box"]["_y"];
				var _width = resizedDetections[i]["_box"]["_width"];
				var _height = resizedDetections[i]["_box"]["_height"];

				var face_ = document.createElement("canvas");
				face_.width = _width;
				face_.height = _height;

				var keys = Object.keys(resizedDetections);
				// console.log(keys); 
				// console.log(resizedDetections[i]["_box"]);
				// console.log(_x + " " + _y + " " + _width + " " + _height);

				var ctx = canvas_video.getContext('2d');
				ctx.drawImage(video, 0, 0);

				var imageData = ctx.getImageData(_x, _y, _width, _height);
				//ctx.putImageData(imageData, 10, 10);
				ctx.putImageData(imageData, 0, 0);
				var ctxx = face_.getContext("2d");
				ctxx.putImageData(imageData, 0, 0);
				image_face.src = face_.toDataURL("image/webp");
				face_data = face_.toDataURL("image/jpeg");
				//alert(face_data);
				//console.log(nbr_face + " detected");
				//faceapi.draw.drawDetections(video, resizedDetections);
				//mage_face.src = ctx.toDataURL('image/webp');
				console.log(nbr_face + " detected");
			}

			if (screen_is === true) {
				canvas_screen.width = screen.videoWidth;
				canvas_screen.height = screen.videoHeight;
				canvas_screen.getContext('2d').drawImage(screen, 0, 0, screen.videoWidth, screen.videoHeight);
				screen_shot.src = canvas_screen.toDataURL('image/webp');
				screen_data = canvas_screen.toDataURL("image/png");
				console.log("width: " + canvas_screen.width);
				console.log("height: " + canvas_screen.height);
				//alert(screen_data);
				// uploadData(face_data,screen_data);
			} else {
				screen_data = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAAA8CAYAAADxJz2MAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAABIuSURBVHhe7Vl7kJbVef/u3+6yu8ByWVAgGTOikERREQEVohFF02k0FjWm1SaxiZqLl1YTM+qgiUanNUmNk8lo06TttDHTpNWAUVBkuSy7wC7ihapRbgJy84aKcvXX3+95znnf71s/O9+k/37M/Hjf9znP7fzOc55zPsjwDxr4f6GmsIH6UVPYQP2oKWygftQUNlA/agobqB81hQ3Uj5rCBupHTWED9aOmsIH6UVPYQP2oKWygftQUNlA/agobqB81hQ3Uj8qPXPKeDaj5ndVTuh+tbzqmVwupXUS0T/0E/9HPR/irikn9bKXvATbV/lNZfP+/4blU2ZvvJIgCF8Izgzyf+QrFAp+C6eYkL9CR9N2hdIXEeU4BNaEg+4g4elbaK4bHpU6WkJ88v/PuL8arjOvf7jdveUlXNoRy5btkmlPBnm4veWKf5Jf6T2BjysdtU3vFTAJEQjzJRDE4TQiUvhGoiUvfA0b9KudGYEg4SbAYEPQq4kXy3Id8i0C+hxxjLpVQbJ+w64sk+5ZNBYEaU761CLTvCv9VBEa5ESj/MT/JlHswKlhwKfHbCIpKejpZycqGcX0nwSxI5YTk3FfdvhO75oAKGZHaBbmRHPIhypQLPu5kWYzEb5S5vsnMj6BcyoaYc0RS8Ym+xqt10pjFATJ7uqBEI61O7eCajBLgU3IidVRtI1lCmunI3scMH0FgtHUdj5kNCWuCTYQTqHixivke7SOB0WfwJ1vfKdX5x3knBJpccd2PxiKchwr/wT7MwR2UwtOCq/Sp5Cshw5iwEnAnifNQ2mnPq6hUg+y99JWs+xPiuCP6M58hpgiUTTJJwWIJfE9ylb6qKxBkhLqNb9lAjAiKdkKI674jeZpnWoFOVMVOirZxXMYRyaRMQQ5l5M/EuVafCScBlLzJKvX4jH5scqpuX6SYVLQfiJiw+xnQc8wvocWKE7EYytMJjyTJl9vGycs26Ff1xhib49G/5ETMR2NVlRpsbVxCezEjOfFmm5AQYQG0OmEr2LfLFSAGS/xU2cbJ8D0Jrol53/XJy68WI9WpeqetYiR+LL7nEOVOkseWzHNKF8K+jWBBscI8w5iPEzVixsMn0UljhY8cJ5AtG9PqNQOdxa3kFRdJ9EqMNqows7GFiOC3wfXNH6FkpK/em7exQQR7o8YLriNfWfNHmK+0FVTCJ+bjvksoEyj3uM18dwKinwQVO0SwWJYj9TgPyeLc9J7wYuTJVoI8E88PY+JD0MzvNqJsTpTMED4HoSXIzbhQon4bjZWYN/gOyjVuSTDpbI46liwhP5lW2rivTL5oE5Sd/BrxmXb6HsoxLk6RiXGbRd82XuR4nj7oN0db5TmI8MWmf41l6TvbYpNsYf5mZ/GGU1YKBHqO7YR8GwmFQuLPdDT3Up7yPOV5k7dlssFfgBWSeMhTMUsC245FV/8mYO9uXHreSeY8m81SuRM3zf0JsGcTJo/j5GTMZC+97Ot45unngUMHgQ/ew46X1+Dmay7HEE4uVnCuVEaOZFnFFkdg4vTzsObZF/HB4f0A3sHWP67BMaOGkUTmkO/A+ONnoO+557H34H4cosahAwdx2w03YGhTM5b0rsFeyhgNhz/Yj62b12LW6ZOYZw6thaFY3tMHef3gA1Og8U5898arkGkehSdWb+DAYRq/zcEDfD+I7175DQwttaJ7zTrsOXgIF39uppFaFEmlJnzr+3Px2sF9+MTwToygTCRacdj8Cca1hcuRWVu5lomY37uZAfbh9Q09+Hh7XPnRuO6We4C3SeAIEToE1975AA4xoU3PP4Obrr8JV1/5t+hZSzI5vduvuhBjGCQJliM5rILOk2ZjK3PfsWEdLj7zBMyeORW/+dW/YGLnWJJQwLnnX4pd7xzAq5s34JYbrsdlX/4a7rrnp/jxPfeirVxGT88idK94EtMmT8fMmWfguY1Pk+g9mDDqWHTmhuLpVV1YuuwRzJh2Ok6fOgMzTzuFE2X1Dx2Px1a/jFVLfo9zTx6LM6Yej2mnTOfkuYOKJLfvJbzPzLf9cQXGtcWqbcKVd3wfr3MZJx05CqMo006x7RvbS9xdVrL5wcgM+jTm9+3A6v5VeGvbU/jtA7dZJWUKo3HjbT8F3tqEUztZSa1jsW4PsHzxgrBl5Yhbs+0o/OLB/wLefA5TO70K/eeXnq049YvXYScL46LZp6KTY6rwAiuvxN7X0ToSO3btxPJlT2J0a8G3lvWnVuRy7SjmmtHfvwxdXX9AKdvEsSJumHst3t73Go4afjQ6Mh3o73kcK7rnoZxXn8zRRrlzi7Ucgz/0bcaapb+nXmgJWlTmnG8agyX9L2Jp32rs2vESfv3Le0PsHL5x9w+xG4dx3PBhGE2ZCMzF019zigR6z2L/aJ2IR9buxpLu5fjiBTNYiBtw4efP4iodge/N/RmwcxOmj2zCmV/6CtaTiL/8/DmWUNEIYn8qHoVZF10JvL8RX5090XqKTvOCkmWLyHYchxd3ANu3voTvfPMiIzinrcC+O3vOZbTbhr86dxJaKW+iz0Ke1W53u2EoM4eunqeIfpwy7WzM+uxsvLbrWfT3zkc520bChmFlfx8WLX/CJifyrI3If/txeGTlLvR0LbQ+LbmqrJX9u419d2XvcnStXI6zL74ce7hDLv2zMyz3q++4FdsPH8TxI49ICMxH8oicneLcws1q2iJw8Kcxr/9VLOl6HM1U+N1//hLr1m9EofNY3HTL3cBrmzClo4Bv3XYXtpDA48cOs95gC1BiBZcn4GOTzwXe24i/v+58D8gVslO6rAbfifEnzMJTzz7DDfMuNm/5H0z/7GeYQCeuufUOYP9GTBkTEiV0UOTstB+GUmkclq9eh72Mu484wIk+cN/3TNdO7vxI9PT3s7upd7LXaVMe2I6x40ZZXgv632JfZPfcv5Vjr+HQ3tfxySOPZc9rRl/vIixma8jkO/HzBx/Dtpf7Maojg2t/eAt2HzyAk0YeiSMYJ54Jto2VnwhUBdrvXxE4aAIWPr0Fz3TP99O2dQyeewO4+xcP4s475gJvvIApI0o476IroFZ8yaxTkq1oLaB0NE79whWc4Xp85axx1nSLhZyXvU4sO0GJQgs+OXkaul/YwB4DnPnnl+GC8+ewfW7AJbPHh0Tp01Zbp/lI+hmOnt6l6OrupZ/x+Kd/6+Ih8SomHKWTvozWto9hZfdSLFk8D9OnT8Wpp0/FSSccTR+MPWgiFvTtQt/iRzF78pE467QJOPGET9mcm1o6sWrtCjy+dCF1O1BuOxJbdm/Cz/71R/jOXbdi7/vv4sQO38LeG/NGXDabNyKNTFVJhn0m0/4pLFi9Ec8u/rVtzUyhE9Mu+juQQyxYOJ8EPo+TR5VQaj0CL21/H08vfRxDqWe9jttIW/3+3zzECnwJJ4+uaLrmn6VeoI5dfVSNrTh6ymewex9w/fU3Y8qkSTjwzias6v5vFEQebez0tsUto6mkSnkCK3qW8nsMcq3HYMfuV7B86TxWa5lbtg1rVnajt5uVRFvdLGxnqAe3TsCjfduxZskC2zHanqruDA+RXGkouvuX4snlC5HToZIZjHPmzMGbrOWHn3wE+/fvwYmdQ6xQrCXwKpTcZ+VHLcKY5YQyrSfgsZVbsO7J32KkyRicJ/B9Dy6zbYO3N2PyaBqyKm68ndca7pQlj3bh61/7Nq765nXoW/ssld7D7dddbAsgAo0M3TF5cp91wRV4aNFanHHOHMyccTbmP/Qwt9R7+IvzZtuWvfkHd9pp+PwL63GDTvavXo37//Fe/PzHd6G9lMEzqxahr3uRtQVV87fn/gPeYg6XfO4LGEz73t4VWNzdjWnTZmHG1NNwzinHYHgHJ9v+CcxbsxP9S5Zh9qTxOPe0yZgxZTI6B41Ae7EFffS7rPvRQA4PQ96H73/wYV6ZDmPv/l2YMKYVQ+h/EOM2ZVoYn/PR/U87xHo4B62P8BrzRO8mPLdknh3bfqEksUMmYuOrLJW3tuP4I5pR4KVT15LLv3wtNm54w+5dhw4dwrYNL2AO71LaukPoWH3UKlB9jASOP+lsXj322D1Of73+yibcceM1RnRZlcJdcOFfX4v1r/CkkRIvgm9u2YJr/uZLvAdmsJoVv7KLJ78Sl9/BH8eGN4Gt617AsSOGoq+vD+/Sxu+BxL4d+I9f3Wc9duG6XdYWtWA4yBf2w9/987+jndtxde8CrFzFw8cWpoPPEci2jcX6ndt4oXsT4zqLdrC188bQQp5U8XZ4KA+70thtusUmoPugSlWT8tNVTLciy18B6k1WVUReP/vCTy/1BJFdon4znaqiBVsAoSACefXQZV1PboESZfLTzDtombDf3oqvXw05b9Syl47yEXRdKsZfA0JJLWFQkpctlmLRV5G7x7ccZU3sz7wllFhhg2gvn0LMM+m30hU5+kXElmC/hihvL3ufF8ok0X9v62YhwrWVjQwxqr3tvxd1G1dw/yWi6sibXBO2wHaPoj7Jl7yl6ImJdJs8e5AfHkoqPAnJTE6dXEjG/0FBOfDKUuAKc1U1KXvKl0GnOX/VsJolK5U0OaLYZOOKneeEom/pFH1yjCvdIuPx5yO/dT2yceon1xI+85KLGO1G26aet/xHsvVuRBvhslfeFoQIl15NqshJKYiUS01i242VQJNde6jLStG4fjPLuSah75zG7XciK4E/iSSTrzK3tXSi36xt26xVsPz5vyN6HGvyAU40F0u7xIj2PAs84eVH0HskxuSyVVXb4njMrOIYQW7jOfApMgKRphfsCrqE27fnbWTzaX3PdLlg1jcVrMKJjG0yuvNQpmpwx2Rc210BTC917hVQ8JNTlSxo+xsxJJhEqaqlV2K1Fkuq7DRmqVSyPpj4irlYRWlCqvbomz6pW6KOVYT0WACeY/i2YqA/klDmPOQzxjMiNZey2hNlReXKfGhXIDleKGG+3EnmVzvR8gnge9w99OWCnJ4UJsmaEzrlmLZmIdnqXpHuxKEqcVtf/VwhVC0TUL+TTlIJhNnH1S/6tyYZe51Vgd0d5dcnnA09yQgkCaaviSkmc5Jf/erJFkWM7w7FlJ793rdYLo95ZlStWiTlzPGYZ0qidAnp2pYOdtKnT+lkNBHdZ6RsRKjKmLyqTwranmreRp4RSCPZ0EkaIBAo5yH5IldW4165quqwomE1C7EKdZLxWz+/rPpImF0pVMGWsHy7jukaqWm1ekPnVlUMvQdCVKEatxxM7j6Ut/VH5Sldvct30LU5c8xIpI33VfmUjYrL/afzD45j8r5V+OS3ZLEqrP/YFqI+4eS4bRVMriqKcFlCwoegmK7nq65YYVKCyQboG8mu7771rQXmWCBQY04wEWTSNWLMRjLFimNpvpVw/9JT3BAj2pgd/5Ki/5O1ElcSUooEst9EY8lrERgWwR3GRByeQMV40EmTiodOHIvjaQzBydWYJiG4vo9XEBgqJCFKPkN+0U9CIOE+BeXy4bhpPhoXFMdjhAMlOlYSIYFQtpKJPP2fsRkpETs902TMUSWSYO7DExiIqKNkQm8d4Cf6j0/fIbKVTarv4yIixgw+TJeoWlzXr0SMkeRc4dflgs/Fi6JCr5rAUDFJMCdU5NkWliwSWLHlEv3EzoM59D0QcaySQL1zzHz4uHzHyQkpgcEuxEtzr5ywj1UhyKNe9C+/bse4oXoF14u5pv6reBKBcTAm606ikTdrT5wy6yU+VplMTM4QE44IcteNthEiw1c08WPbWYvnJEafccJmZ31Q9v4d83AfwXcSP3zbWKoXkczbbFK7qlzNT9SvIFAFlQ5ER2lyHw4QxoLDqOPjQacSIZmom6JaryqOxVBlOonRRzoeEL8D9O7f0W+UVeec6hGUJd9h4VK9QFS0Dboi0M6KVB6dyEADVYM2ZsHiWJC7LEW0T31U+xlolyboMaq2qJEYEHZEkod81OrBleOExnx7Rl9pLPOl6on9Ud+xt1peTpTlp/EKPZHn/w2bzC04tABOQGIYkYxXyE2WopLAqqSlGxOosPO48unvVa3C2kS057OibUR/0k0IlP/E1mVOoOc0kMBKP/Y0mXTSyo99daBectjGWG4cUZHon4RoP8BPkuRHwwkN36Yf7fmstNc78WH9aqTjA3KJCH5SWdRz3cS3UKHni17lLx1s4E9CTWED9aOmsIH6UVPYQP2oKWygftQUNlA/agobqB81hQ3Uj5rCBupHTWED9aOmsIG6kMH/ArLczal/NW8vAAAAAElFTkSuQmCC"
			}
			requestXHR(face_data, screen_data);

		} else {
			console.log(nbr_face + " detected");
			image_face.src = "images/no_face.png";
			face_data = null;
		}
	})
	/**
	 * check if the use give permission to access
	 * his creen with displaySurface == "monitor" for chrome
	 */
	async function shareScreen() {

		try {
			screen.srcObject = await navigator.mediaDevices.getDisplayMedia(display_media_options);
			dumpOptionsInfo();
		} catch (err) {
			console.log("Access denide screen");
		}
	}

	function dumpOptionsInfo() {
		const videoTrack = screen.srcObject.getVideoTracks()[0];
		console.info("Track settings:");
		console.info(JSON.stringify(videoTrack.getSettings(), null, 2));
		console.info("Track constraints:");
		console.info(JSON.stringify(videoTrack.getConstraints(), null, 2));
		display_surface = videoTrack.getConstraints().mediaSource();
	}

	function requestXHR(face, screen) {

		var dt = new Date();
		var date = `${dt.getFullYear().toString().padStart(4, '0')}-
			${(dt.getMonth()+1).toString().padStart(2, '0')}-
			${dt.getDate().toString().padStart(2, '0')}${
			dt.getHours().toString().padStart(2, '0')}:${
			dt.getMinutes().toString().padStart(2, '0')}:${
			dt.getSeconds().toString().padStart(2, '0')}`;
		console.log(date);

		if (face == null) {
			status = "only screen, no face";
			console.log(status);
		}
		const toSend = {
			//studentId: "205657", //steven
			// studentId: "03214",// nelka
			studentId: "XXXX", // anh Tuấn
			status: status,
			timeInout: date,
			screenStatus: "1",
			imageInout: face,
			inoutState: "1",
			roomId: "<?php echo $_SESSION["roomId"]; ?>",
			schoolId: "<?php echo $_SESSION["schoolId"]; ?>",
			nvrId: "1",
			cameraId: "9",
			macAddr: "99:99:XX:XX:XX:XX",
			screen: screen
		};
		const jsonString = JSON.stringify(toSend);
		// console.log(jsonString)
		var xhr = new XMLHttpRequest();
		const url = "https://27.71.228.53:9004/SmartClass/student/facescreen";
		console.log("sent API")
		xhr.open("POST", url);
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.send(jsonString);

		xhr.addEventListener('readystatechange', function() {
			if (xhr.readyState === XMLHttpRequest.DONE) {
				console.log(xhr.status)
				console.log(xhr.status)
				var response = xhr.responseText;
				console.log(response);
			}
		});
	}
	/**
	 * 
	 */
</script>
<script defer src="js/face_api.min.js"></script>
<script defer src="js/jquery-3.4.1.min.js"></script>


</html>