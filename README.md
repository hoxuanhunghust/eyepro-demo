# Cài đặt:
1. Cài đặt Xampp (https://www.apachefriends.org/index.html)
2. Bỏ thư mục eyepro-demo vào thư mục htdocs của Xampp
3. Chú ý để gửi thông tin nào lên server cần điều chỉnh thông tin tại Dòng 596 file <b>main.php</b>:

```
const toSend = {
    //studentId: "205657", //steven
    // studentId: "03214",// nelka
    studentId: "XXXX", // anh Tuấn
    status: status,
    timeInout: date,
    screenStatus: "1",
    imageInout: face,
    inoutState: "1",
    roomId: "<?php echo $_SESSION["roomId"]; ?>",
    schoolId: "<?php echo $_SESSION["schoolId"]; ?>",
    nvrId: "1",
    cameraId: "9",
    macAddr: "99:99:XX:XX:XX:XX",
    screen: screen
};
```

Và Dòng 615: ``` const url = "https://27.71.228.53:9004/SmartClass/student/facescreen"; ```

4. Truy cập: https://localhost/eyepro-demo

<i>Chú ý: cần sử dụng giao thức HTTPS mới có thể dùng webcam</i>
